
from ioconsole.ABE_helpers import ABEHelpers
from ioconsole.ABE_ADCPi import ADCPi


class Input(object):

    pins = range(1, 9)
    
    def __init__(self):
        i2c_helper = ABEHelpers()
        bus = i2c_helper.get_smbus()
        
        self.adc = ADCPi(bus, 0x6A, 0x6B, 16)

    def calcCurrent(self, voltage):
        return ((voltage) * 1)
        
    def getPort(self, channel):
        output = self.adc.read_voltage(channel)
        return calcCurrent(output)

    def getAll(self):
        output = []
        for pin in self.pins:
            output.append(self.adc.read_voltage(pin))
        return output
