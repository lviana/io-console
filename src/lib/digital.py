
from ioconsole.ABE_helpers import ABEHelpers
from ioconsole.ABE_IoPi import IoPi


class Output(object):

    pins = range(1, 17) + range(1,17)

    def __init__(self):
        i2c_helper = ABEHelpers()
        i2c_bus = i2c_helper.get_smbus()
        
        self.bus0 = IoPi(i2c_bus, 0x26)
        self.bus1 = IoPi(i2c_bus, 0x27)

        self.bus0.set_port_direction(0, 0x00)
        self.bus0.set_port_direction(1, 0x00)
        self.bus1.set_port_direction(0, 0x00)
        self.bus1.set_port_direction(1, 0x00)

    def setPort(self, pin=1, state=1):
        if pin <= 16:
            self.bus0.write_pin(self.pins[pin-1], state)
        else:
            self.bus1.write_pin(self.pins[pin-1], state)

    def setPortsOn(self):
        self.bus0.write_port(0, 0xff)
        self.bus0.write_port(1, 0xff)
        self.bus1.write_port(0, 0xff)
        self.bus1.write_port(1, 0xff)

    def setPortsOff(self):
        self.bus0.write_port(0, 0x00)
        self.bus0.write_port(1, 0x00)
        self.bus1.write_port(0, 0x00)
        self.bus1.write_port(1, 0x00)

    def getPorts(self):
        b0 = [self.bus0.read_pin(x) for x in range(1,16)]
        b1 = [self.bus1.read_pin(x) for x in range(1,16)]
        return b0 + b1

    def setAll(self, plist):
        for pin, state in enumerate(plist):
            self.setPort(pin, state)
