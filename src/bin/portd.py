#!/usr/bin/python
# -*- coding: utf-8 -*-

from bottle import request, abort
from bottle import route, template, static_file

from datetime import timedelta

from ioconsole import digital
from ioconsole import analog

import bottle
import logging
import os
import time
import sys
import syslog
import pystache


syslog.openlog('portd', syslog.LOG_PID, syslog.LOG_SYSLOG)

app = application = bottle.Bottle()


@app.get('/digital')
@app.get('/digital/:pin')
def getDigital(pin='all'):
    # To-do!
    if not isinstance(pin, int):
        state = [None] * 32
    else:
        state = [True]
    return { 'state': state }


@app.post('/digital')
def setAllDigital():
    entity = request.json
    state = entity.get('state')

    if len(state) >= 33:
        abort(400, "Malformed object, [(pin, state), (pin, state)]")

    dg = digital.Output()
    dg.setAll(state)
    del dg
    
    return { 'state': state }


@app.post('/digital/:pin/:state')
def setDigital(pin, state):
    if int(pin) not in range(1, 33):
        abort(400, "Invalid pin informed, 1-32 only")
        
    if int(state) not in [0, 1]:
        abort(400, "Invalid state informed, 0 or 1")

    dg = digital.Output()
    dg.setPort(int(pin), int(state))
    del dg

    return "Pin %d, State %d" % (pin, state)


@app.get('/analog')
@app.get('/analog/:pin')
def getAnalog(pin='all'):
    an = analog.Input()

    if not isinstance(pin, int):
        state = an.getAll()
    else:
        state = an.getPort(pin)
    del an

    return { 'state': state }


class StripPathMiddleware(object):
    def __init__(self, a):
        self.a = a
    def __call__(self, e, h):
        e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self.a(e, h)
    
def main():
    bottle.debug(True)
    syslog.syslog(syslog.LOG_INFO, 'Starting portd service')
    bottle.run(app=StripPathMiddleware(app),
               server='auto',
               host='0.0.0.0',
               port=80)


if __name__ == '__main__':
    main()
