#!/usr/bin/python

from ioconsole import baker

import subprocess
import sys
import urllib
import urllib2


query_args = { 'q':'query string', 'foo':'bar' }
encoded_args = urllib.urlencode(query_args)
url = 'http://localhost:8080/'
print urllib2.urlopen(url, encoded_args).read()

@baker.command
def digital(port=1):
    """ Pulse a specific digital port.
    """
    if port is not in range(1,33):
        print('Digital port not available, port range 1-32')
        sys.exit(1)
    args = { 'action': 'pulse' }
    eargs = urllib.urlencode(args)
    url = 'http://localhost:8000/digital/%s' % port
    output urllib2.urlopen(url, eargs).read()
    print('200, sent pulse request')

baker.run()
