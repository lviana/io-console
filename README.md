IOconsole power management
==========================

Management console and REST api for POE management, no-break optimization and electrical monitoring.

Used in wireless repeating stations, home automation and no-break optimization on commercial buildings.

Available resources
-------------------

GET /digital

GET /digital/<pin number>

POST /digital

POST /digital/<pin number>/<state>

GET /analog

GET /analog/<pin number>


Call examples
-------------

Change state on digital port 11 to 1 (on):

$ curl -X POST http://ioconsole.local/digital/11/1

Get current value on analog port 4:

$ curl http://ioconsole.local/analog/4