#!/usr/bin/python
# -*- coding: utf-8 -*-

from bottle import request
from bottle import route, template, static_file

from ABE_helpers import ABEHelpers
from ABE_IoPi import IoPi
from ABE_ADCPi import ADCPi

from datetime import timedelta

import RPi.GPIO as GPIO

import bottle
import logging
import os
import time
import sys
import syslog
import pystache


syslog.openlog('portd', syslog.LOG_PID, syslog.LOG_SYSLOG)

app = application = bottle.Bottle()


""" Log management engine
"""

logdir = '/var/log/portd'
eventlog = logdir + '/events.log'

if not os.path.isdir(logdir):
    os.makedirs(logdir)

logging.basicConfig(filename=eventlog
                    level=logging.DEBUG
                    format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S')


""" Management interface routes
"""

@route('/')
@route('/dashboard')
def dashboard():
    # getting digital port states
    digital = DigitalOutput()
    dports = digital.getPorts()
    del digital
    
    # Getting analog port states
    analog = AnalogInput(1.0)
    aports = analog.getPorts()
    del analog

    # Getting system uptime / time
    now = time.strftime("%c")
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        uptime_string = str(timedelta(seconds = uptime_seconds))

    # Generating html output
    output = template('static/index.tpl',
                      dports=dports,
                      aports=aports,
                      clock=now,
                      uptime=uptime_string)
    return output


@route('/logs')
def showlogs(allentries=False):
    # Reading log lines
    output = []
    for line in open(eventlog, 'r').readlines():
        entries = {}
        entries['date'] = line.split()[0]
        entries['time'] = line.split()[1]
        entry = ''
        for i in line.split()[2:len(line.split())]:
            entry = entry + i + ' '
        entries['entry'] = entry.rstrip()
        output.append(entries)
    output.reverse()
    if not allentries:
        return output[:100]
    else:
        return output

    
@route('/logs/clear')
def clearlogs():
    # Clear log file
    open(eventlog, 'w').close()
    return 'Cleared'


@route('/admin')
def showadmin():
    # Show administration interface
    return 'Need to create administration template'


@route('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root='/home/lviana/code/port-manager/static')

""" Management API routes
"""

def get_digital_state():
    """ Get current digital port states
    """
    # Need to read file with the current values
    return { 'digital': [True, True, False, True] }


@app.get('/analog')
def get_analog_state():
    """ Get current analog port states
    """
    analog = AnalogInput(1.0)
    output = analog.getPorts()
    del analog
    return { 'analog': output }


@app.post('/digital')
def set_digital_state():
    """ Set all digital ports states
    """
    entity = request.json
    state = entity.get('state')
    
    digital = DigitalOutput()
    digital.setPorts(state)
    del digital
    logging.info('Digital port states set: %s' % state)
    return { 'digital': state }


@app.post('/pulse/<port>')
def set_pulse(port):
    logging.info('Pulse sent to port %s' % port)
    digital = DigitalOutput()
    if digital.getPort(port) is False:
        return 'Port currently not active, skipping'
    digital.setPort(int(port), 0)
    time.sleep(10)
    digital.setPort(int(port), 1)
    del digital
    return 'Port pulse completed, port %s' % port


class StripPathMiddleware(object):
    def __init__(self, a):
        self.a = a
    def __call__(self, e, h):
        e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self.a(e, h)
    
def main():
    bottle.debug(True)
    syslog.syslog(syslog.LOG_INFO, 'Starting portd service')
    bottle.run(app=StripPathMiddleware(app),
               server='auto',
               host='0.0.0.0',
               port=8081)


if __name__ == '__main__':
    main()


class DigitalOutput(object):

    self.pins = range(1, 17) + range(1,17)

    def __init__(self):
        i2c_helper = ABEHelpers()
        i2c_bus = i2c_helper.get_smbus()
        
        self.bus0 = IoPi(i2c_bus, 0x20)
        self.bus1 = IoPi(i2c_bus, 0x21)
        self.bus0.set_port_direction(0, 0x00)
        self.bus0.set_port_direction(1, 0x00)
        self.bus1.set_port_direction(0, 0x00)
        self.bus1.set_port_direction(1, 0x00)

    def setPort(self, pin=1, state=1):
        if pin <= 16:
            self.bus0.write_pin(self.pins[pin-1], state)
        else:
            self.bus1.write_pin(self.pins[pin-1], state)

    def getPort(self, pin=1):
        if pin <= 16:
            return self.bus0.read_pin(pin)
        else:
            return self.bus1.read_pin(pin)

    def setPortsOn(self):
        self.bus0.write_port(0, 0xff)
        self.bus0.write_port(1, 0xff)
        self.bus1.write_port(0, 0xff)
        self.bus1.write_port(1, 0xff)

    def setPortsOff(self):
        self.bus0.write_port(0, 0x00)
        self.bus0.write_port(1, 0x00)
        self.bus1.write_port(0, 0x00)
        self.bus1.write_port(1, 0x00)

    def getPorts(self):
        b0 = [self.bus0.read_pin(x) for x in range(1,17)]
        b1 = [self.bus1.read_pin(x) for x in range(1,17)]
        return b0 + b1

    def setPorts(self, plist):
        for pin, state in enumerate(plist):
            self.setPort(pin, state)

            
class AnalogInput(object):

    """ ACS 712
    """
    self.ports = [None] * 8
    Port(self, port=1):
        rawvcc = self.adc.read_voltage(port)
        return rawvcc * self.fm

    def getPorts(self):
        for port in range(1, 9):
            self.ports[port] = self.read(port)
        return self.ports
