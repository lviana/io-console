%#Dashboard template
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Power switch controller</title>

    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="static/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
      // Popup window code
      function newPopup(url) {
      popupWindow = window.open(
      url,'popUpWindow','height=300,width=400,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no')
      }
    </script>
</head>

<body>

  <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Power Switch Console</a>
      </div>

      <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
        <span class="label label-default"> Administrator</spam>
      </ul>

      <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
          <li class="active">
            <a href="/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
          </li>
	  <li>
            <a href="/logs"><i class="fa fa-fw fa-table"></i> Logs</a>
          </li>
          <li>
            <a href="/config"><i class="fa fa-fw fa-edit"></i> Configuration</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              Dashboard <small>overview</small>
            </h1>
          </div>
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-lg-8">
            <div class="panel panel-default">
              <div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i> Digital Ports</h3>
              </div>
	      <div class="table-responsive">
		<table class="table table-hover table-striped">
		  <tbody>
		    <% counter = 1 %>
		    %for port in dport:
		    <tr>
		      <td><strong>Port {{counter}}</strong></td>
		      <td>
			<label class="radio-inline">
			  %if port is True:
			  <input type="radio" name="digital{{counter}}" align="middle" value="1" checked>On
			  %end
			  %if port is False:
			  <input type="radio" name="digital{{counter}}" align="middle" value="1">On
			  %end
			</label>
			<label class="radio-inline">
			  %if port is False:
			  <input type="radio" name="digital{{counter}}" align="middle" value="2" checked>Off
			  %end
			  %if port is True:
			  <input type="radio" name="digital{{counter}}" align="middle" value="2">Off
			  %end
			</label>
		      </td>
		      <td>
			<a href="JavaScript:newPopup('/pulse/{{counter}}');">pulse</a>
		      </td>
		      <td>
			%if state[counter] == 1:
			<label class="radio-inline">
			  <input type="radio" name="associate{{counter}}" align="middle" value="1" checked>Analog 1
			</label>
			<label class="radio-inline">
			  <input type="radio" name="associate{{counter}}" align="middle" value="2">Analog 2
			</label>
			%end
			%if state[counter] == 2:
			<label class="radio-inline">
			  <input type="radio" name="associate{{counter}}" align="middle" value="1">Analog 1
			</label>
			<label class="radio-inline">
			  <input type="radio" name="associate{{counter}}" align="middle" value="2" checked>Analog 2
			</label>
			%end
		      </td>
		    </tr>
		    <% counter = counter + 1 %>
		    %end
		  </tbody>
		</table>
	      </div>
	    </div>
	  </div>

	  <div class="col-lg-4">
	    
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i> System</h3>
              </div>
              <div class="panel-body">
		<strong>Clock:</strong> {{ clock }}<br />
		<strong>Uptime:</strong> {{ uptime }}
	      </div>
	    </div>
	     
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i> Controls</h3>
              </div>
              <div class="panel-body">
		<button type="button" class="btn btn-primary">Save changes</button>
		<button type="button" class="btn btn-danger">Reset ports</button>
	      </div>
	    </div>

	    <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i> Analog</h3>
              </div>
              <div class="panel-body">
		<% counter = 1 %>
	      	%for a in aport:
		Analog input {{counter}}: <strong>{{a}} Vcc</strong><br />
		<% counter = counter +1 %>
		%end
	      </div>
	    </div>
	     
	  </div>

	  <div class="col-lg-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Latest Events</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped">
                    <tbody>
                      <tr>
                        <td>[Alert] Analog 1, Normal voltage detected (xx.xx)</td>
                      </tr>

		      <!-- Fill out automatically -->
                      
		      <tr>
                        <td>[Alert] Analog 1, Low voltage detected (xx.xx)</td>
                      </tr>

		      <!-- end of fill out automatically -->

                    </tbody>
                  </table>
                </div>
                <div class="text-right">
                  <a href="logs">View All Events <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
	  </div>
		  
	</div>
	<!-- /.row -->
	
      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- jQuery -->
  <script src="static/js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="static/js/bootstrap.min.js"></script>

  <!-- Morris Charts JavaScript -->
  <script src="static/js/plugins/morris/raphael.min.js"></script>
  <script src="static/js/plugins/morris/morris.min.js"></script>
  <script src="static/js/plugins/morris/morris-data.js"></script>

</body>

</html>
