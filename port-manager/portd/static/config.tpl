%#Log viewer template
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Power switch - configuration</title>

    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="static/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
      // Popup window code
      function newPopup(url) {
      popupWindow = window.open(
      url,'popUpWindow','height=300,width=400,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no')
      }
    </script>

</head>

<body>

  <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Power Switch Console</a>
      </div>

      <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
        <span class="label label-default"> Administrator</spam>
      </ul>

      <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
          <li>
            <a href="/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
          </li>
	  <li>
            <a href="/logs"><i class="fa fa-fw fa-table"></i> Logs</a>
          </li>
          <li class="active">
            <a href="/config"><i class="fa fa-fw fa-edit"></i> Configuration</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              Dashboard <small>configuration</small>
            </h1>
          </div>
        </div>
        <!-- /.row -->

        <div class="row">
	  <div class="col-lg-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i> New address</h3>
              </div>
              <div class="panel-body">
		<form action="/config/apply">
		    <div class="form-group">
		      <label for="inputIP">IP address (ipv4 only)</label>
		      <input name="ipaddr" type="text" class="form-control" id="inputIP" placeholder="IPv4 address">
		    </div>
		    <div class="form-group">
		      <label for="inputMask">Netmask</label>
		      <input name="netmask" type="text" class="form-control" id="inputMask" placeholder="Network mask">
		    </div>
		    <div class="form-group">
		      <label for="inputGateway">Gateway</label>
		      <input name="gateway" type="text" class="form-control" id="inputGateway" placeholder="Default gateway">
		    </div>
		    <div class="form-group">
		      <label for="inputDNS1">DNS 1:</label>
		      <input name="dns1" type="text" class="form-control" id="inputDNS1" placeholder="Primary resolver">
		    </div>
		    <div class="form-group">
		      <label for="inputDNS2">DNS 2:</label>
		      <input name="dns2" type="text" class="form-control" id="inputDNS2" placeholder="Secondary resolver">
		    </div>
		    <div class="form-group">
		      <label for="inputName">Hostname:</label>
		      <input name="hostname" type="text" class="form-control" id="inputName" placeholder="Hostname / FQDN">
		    </div>
		  <button type="submit" class="btn btn-default">Apply</button>
		</form>
              </div>
            </div>
	  </div>

	  <div class="col-lg-4">
	    
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i> Controls</h3>
              </div>
              <div class="panel-body">
		<form action="/">
		  <input type="submit" class="btn btn-danger" value="Restore defaults"/>
		  <p class="help-block"><br/>Default address: 192.168.0.2/24</p>
		</form>
	      </div>
	    </div>

	  </div>
		  
	</div>
	<!-- /.row -->

      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- jQuery -->
  <script src="static/js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="static/js/bootstrap.min.js"></script>

  <!-- Morris Charts JavaScript -->
  <script src="static/js/plugins/morris/raphael.min.js"></script>
  <script src="static/js/plugins/morris/morris.min.js"></script>
  <script src="static/js/plugins/morris/morris-data.js"></script>

</body>

</html>
