#!/usr/bin/python
# -*- coding: utf-8 -*-

from bottle import route, template, static_file
from datetime import timedelta
from Cheetah.Template import Template

import bottle
import pystache
import time
import socket
import subprocess


aport = [1.2, 1.3, 1.5, 2.0, 1.2, 1.2, 1.4, 2.1]
dport = [True, True, False, True, True, False, True, True, True, True, True, False, True, False, False, True, True, True, False, True, True, False, True, True, True, True, True, False, True, False, False, True]
state = { 1: 1, 2: 1, 3: 2, 4: 1, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1, 10: 1, 11: 1, 12: 1, 13: 1, 14: 2, 15: 1, 16:1, 17: 1, 18: 1, 19: 2, 20: 1, 21: 2, 22: 1, 23: 1, 24: 1, 25: 1, 26: 1, 27: 1, 28: 1, 29: 1, 30: 2, 31: 1, 32: 1 }
logs = ['[alert] Analog input 1, low voltage detected',
        '[alert] Analog input 1, normal voltage detected',
        '[info] Port 32 state changed, 0',
        '[info] Port 1 state changed, 1',
        '[info] IP address updated, 192.168.1.2']


app = application = bottle.Bottle()

@route('/')
def dash():
    now = time.strftime("%c")
    with open('/proc/uptime', 'r') as f:
        uptime_sec = float(f.readline().split()[0])
        uptime_str = str(timedelta(seconds = uptime_sec))

    output = template('static/index.tpl',
                      clock=now,
                      uptime=uptime_str,
                      aport=aport,
                      dport=dport,
                      state=state)

    return output


@route('/logs')
def getlogs():
    now = time.strftime("%c")
    with open('/proc/uptime', 'r') as f:
        uptime_sec = float(f.readline().split()[0])
        uptime_str = str(timedelta(seconds = uptime_sec))

    output = template('static/logs.tpl',
                      clock=now,
                      uptime=uptime_str,
                      logs=logs)
    return output


@route('/config')
def config():
    now = time.strftime("%c")
    with open('/proc/uptime', 'r') as f:
        uptime_sec = float(f.readline().split()[0])
        uptime_str = str(timedelta(seconds = uptime_sec))

    output = template('static/config.tpl',
                      clock=now,
                      uptime=uptime_str,
                      logs=logs)

    return output

@route('/config/apply')
def applynetcfg():
    return True


@route('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root='static')


def nsetup(hname, address, netmask, gateway, dns1, dns2):
    """ Modify operating system network config
    """
    for i in [address, netmask, gateway]:
        if evaladdr(i) is False:
            return False
    ifcfg = {'addr': address,
             'mask': netmask,
             'gate': gateway,
    }
    render({'config': ifcfg}, '/etc/network/interfaces','iface.tpl')
    with open('/etc/hostname', 'w') as hnamef:
        hnamef.write(hname)

def render(attr, cfile, tmpl):
    render = Template(file="%s" % template, searchList=[attr])
    open(cfile, 'w').write(str(render))

def evaladdr(ipaddr):
    try:
        socket.inet_aton(ipaddr)
    except socket.error:
        return False

def main():
    bottle.debug(True)
    bottle.run(server='auto', reloader=True, host='127.0.0.1', port=8000)

if __name__ == '__main__':
    main()
