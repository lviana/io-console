#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import logging
import os
import requests
import sched
import syslog
import time

from ABE_ADCPi import ADCPi
from ABE_helpers import ABEHelpers


###
# Set checking interval (seconds)
interval = 5

# Set multiplier factor
multiplier = 1.0

# Set threshold limits
cmin = float(2)
cmax = float(4)

# Enable / disable debug mode
debug = True
###

# TODO: Adapt the current service to collect data from daemon

syslog.openlog('anacheck', syslog.LOG_PID, syslog.LOG_SYSLOG)


""" Log management engine
"""

logdir = '/var/log/portd'
eventlog = logdir + '/events.log'

if not os.path.isdir(logdir):
    os.makedirs(logdir)

logging.basicConfig(filename=eventlog
                    level=logging.DEBUG
                    format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


class a(object):
    state = [None] * 8


def 
def check(sc):
    r = requests.get('http://localhost/analog')
    state = r.json()

    if last[0] is not None:
        
        if state['analog'] != last.state:
            alert = [ i for i,x in enumerate(state) if x < last.state[i] or x > last.state[i] ]
            payload = { 'state': alert }
            r = requests.post('http://localhost/digital', data=payload)
            logging.info('Alert - Port threshold reached: %s' % alert)

    last.state = state['analog']
    sc.enter(interval, 1, check, (sc,))

    
last = a()

s = sched.scheduler(time.time, time.sleep)
s.enter(interval, 1, check, (s,))

syslog.syslog(syslog.LOG_INFO, 'Starting anacheck scheduler')
s.run()
