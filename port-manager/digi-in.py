#!/usr/bin/python

import RPi.GPIO as GPIO
import subprocess

gpios = [4, 5, 6, 7]

for pin in gpios:
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(pin, GPIO.BOTH)
    GPIO.add_event_callback(pin, disable_ping)

def disable_ping():
    print("Disabled icmp response")
    subprocess.call(["/usr/bin/iptables", "-A", "INPUT", "-p", "icmp", "--icmp-type", "echo-request", "-j", "REJECT"])    

