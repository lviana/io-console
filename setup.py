
from setuptools import setup

setup(name='ioconsole',
      version='0.1.0',
      description='Digital IO controller',
      author='Luiz Viana',
      author_email='lviana@include.io',
      maintainer='Luiz Viana',
      maintainer_email='lviana@include.io',
      url='https://bitbucket.org/lviana/io-console',
      license='Apache',
      packages=['ioconsole'],
      package_dir={'ioconsole': 'src/lib'},
      scripts=['src/bin/portd'],
)
